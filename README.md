A simple showroom demo that demonstrates the capabilities of my car configurator. There is a Unity project as well as a Unity package.

Implemented parts:
* Orbital camera + Zoom
* Paint change
* Wheels, spoilers and exhaust change
* Option to turn on lights
* 2 shader graphs
* Asset Bundles loader (local)
* Change view (interior/exterior)
* Subtle camera shake
* Price and performance based on vehicle equipment
* Screenshot option 
* Sound 

Assets used:
* SimpleShowroomAsset (only as environment)
* 2 graph shaders (5 in total, 1 Unity, 2 mine, 2 Volvo asset)
* Cinemachine
* HQ Racing Car Model No.1203 
* Universal Accessories

To start the project, open the Assets\SkalaDev\Scenes ShowRoomDemo scene and add the ShowRoomDemoLighting scene

Video: https://youtu.be/00XS5Te89oc
