using System;
using System.Collections;
using UnityEngine;

public enum CarPartType
{ 
    wheels, 
    muffler, 
    spoiler
}

public class CarTuning : MonoBehaviour
{
    private int carSelected = 0;
    private int wheelsSelected = 0;
    private int mufflerSelected = 0;
    private int spoilerSelected = 0;

    public GameObject car;
    private GameObject currentWheels;
    private GameObject currentMuffler;
    private GameObject currentSpoiler;

    public void Start()
    {
        //Set default parts
        SetWheels(wheelsSelected);
        SetMuffler(mufflerSelected);
        SetSpoiler(spoilerSelected);
    }

    public void SetCar(int carIndex)
    {
        carSelected = carIndex;
    }

    public void SetWheels(int index)
    {
        //Debug.Log("Setting wheels index: " + index);
        PlayerPrefs.SetInt(carSelected + "_Wheels", index);
        
        var result = SpawnNewGameObject(CarPartType.wheels, index, currentWheels);
        if (result != null)
        {
            currentWheels = Instantiate(result, car.transform);
            CarStatsManager.Instace.AdjustPartStats(currentWheels);
        }
        else
        {
            if (index != 0)
            {
                //Debug.Log("No more wheels to load, selecting default wheels");
                currentWheels = null;
                wheelsSelected = 0;
                SetWheels(wheelsSelected);
            }
        }
    }
    
    public void SetMuffler(int index)
    {
        //Debug.Log("Setting muffler index: " + index);
        PlayerPrefs.SetInt(carSelected + "_Muffler", index);
        
        var result = SpawnNewGameObject(CarPartType.muffler, index, currentMuffler);
        if (result != null)
        {
            currentMuffler = Instantiate(result, car.transform);
            CarStatsManager.Instace.AdjustPartStats(currentMuffler);
        }
        else
        {
            if (index != 0)
            {
                //Debug.Log("No more mufflers to load, selecting default wheels");
                currentMuffler = null;
                mufflerSelected = 0;
                SetMuffler(mufflerSelected);
            }
        }
    }

    public void SetSpoiler(int index)
    {
        //Debug.Log("Setting spoiler index: " + index);
        PlayerPrefs.SetInt(carSelected + "_Spoiler", index);
        
        var result = SpawnNewGameObject(CarPartType.spoiler, index, currentSpoiler);
        if (result != null)
        {
            currentSpoiler = Instantiate(result, car.transform);
            CarStatsManager.Instace.AdjustPartStats(currentSpoiler);
        }
        else
        {
            if (index != 0)
            {
                //Debug.Log("No more spoilers to load, selecting default wheels");
                currentSpoiler = null;
                spoilerSelected = 0;
                SetSpoiler(spoilerSelected);
            }
        }
    }
    
    public GameObject SpawnNewGameObject(CarPartType type, int index, GameObject part)
    {
        if (part != null)
        {
            //Adjust car stats
            CarStatsManager.Instace.RemoveGain(part);
            //Destroy old part
            Destroy(part);
        }

        part = BundledObjectLoader.Instace.LoadBundleAsGameObject(type, index);
        if (part == null)
        {
            if (index == 0)
            {
                Debug.LogWarning("No "+type+" to load");
            }
        }
        return part;
    }

    public void SetNextWheels()
    {
        wheelsSelected++;
        SetWheels(wheelsSelected);
    }

    public void SetNextMuffler()
    {
        mufflerSelected++;
        SetMuffler(mufflerSelected);
    }

    public void SetNextSpoiler()
    {
        spoilerSelected++;
        SetSpoiler(spoilerSelected);
    }
}
