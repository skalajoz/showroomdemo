using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class BundledObjectLoader : MonoBehaviour
{
    private string bundleName;
    private string assetName;
    
    //Singleton
    public static BundledObjectLoader Instace { get; private set; }

    private void Awake()
    {
        if (Instace != null && Instace != this)
        {
            Destroy(this);
        }
        else
        {
            Instace = this; 
        }
    }
    public GameObject LoadBundleAsGameObject(CarPartType type, int index)
    {
        InitAssetDescription(type);
        
        AssetBundle localAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, bundleName));

        if (localAssetBundle == null)
        {
            Debug.LogError("Failed to load AssetBundle: " + bundleName);
            return null;
        }

        GameObject asset = localAssetBundle.LoadAsset<GameObject>(assetName + index);
        localAssetBundle.Unload(false);

        return asset;
    }

    private void InitAssetDescription(CarPartType type)
    {
        switch (type)
        {
            case CarPartType.wheels:
                bundleName = "CarPartsPrefabs/Wheels";
                assetName = "Wheels";
                break;
            case CarPartType.muffler:
                bundleName = "CarPartsPrefabs/Exhaust";
                assetName = "Exhaust";
                break;
            case CarPartType.spoiler:
                bundleName = "CarPartsPrefabs/Spoiler";
                assetName = "Spoiler";
                break;
        }
    }
}
