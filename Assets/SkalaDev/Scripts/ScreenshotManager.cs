using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScreenshotManager : MonoBehaviour
{
    public MenuConfigurator menuConfigurator;
    public string ScreenCapDirectory = "C:\\Users\\YourUserNameGoesHere\\Documents\\";

    private void Start()
    {
        ScreenCapDirectory = Application.persistentDataPath;
    }

    public void TakeScreenshot()
    {
        StartCoroutine(DoCapture());
        //TODO Disable UI
    }

    IEnumerator DoCapture()
    {
        yield return null;
        yield return new WaitForEndOfFrame();

        string currentScreenshot = "ShowroomDemoSkalaDev-Screenshots" + DateTime.Now.ToLongTimeString().Replace(" ", "_").Replace(":", "_") + ".png";
        ScreenCapture.CaptureScreenshot(ScreenCapDirectory + currentScreenshot);

        Debug.Log("ScreenCapture Taken!");
        Debug.Log("ScreenCap Location: " + ScreenCapDirectory);
        
        //TODO Enable  UI
    }
}
