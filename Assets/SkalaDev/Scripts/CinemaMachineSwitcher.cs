using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemaMachineSwitcher : MonoBehaviour
{
    private Animator _animator;
    
    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void SwitchToOrbitalState()
    {
        _animator.Play("OrbitalCamera");
    }
    
    public void SwitchToInteriorState()
    {
        _animator.Play("InteriorCamera");
    }
}
