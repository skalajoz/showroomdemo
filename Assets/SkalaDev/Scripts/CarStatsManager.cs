using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarStatsManager : MonoBehaviour
{
    //Singleton
    public static CarStatsManager Instace { get; private set; }
    public Text topSpeedText;
    public Text accelerationText;
    public Text weightText;
    public Text priceText;

    //Default Values
    private int _currentTopSpeed = 250;
    private float _currentAcceleration = 5.0f;
    private int _currentWeight = 800;
    private int _currentPrice = 50000;
    
    private void Awake()
    {
        if (Instace != null && Instace != this)
        {
            Destroy(this);
        }
        else
        {
            Instace = this; 
        }
    }
    
    void Start()
    {
        //Set Default values
        topSpeedText.text = _currentTopSpeed + " km/h" ;
        accelerationText.text = _currentAcceleration+ " s";
        weightText.text = _currentWeight+ " kg";
        priceText.text = _currentPrice + " Euro";
    }

    public void AdjustPartStats(GameObject part)
    {
        CarPartsStats carPartsStats = part.GetComponent<CarPartsStats>();
        if (carPartsStats == null)
        {
            Debug.LogWarning("Car part does not have data");
            return;
        }
        
        //Set values
        _currentTopSpeed += carPartsStats.topSpeedGain;
        _currentAcceleration += carPartsStats.accelerationGain;
        _currentWeight += carPartsStats.weightGain;
        _currentPrice += carPartsStats.priceGain;

        //Set UI
        topSpeedText.text = _currentTopSpeed + " km/h";
        accelerationText.text = System.Math.Round(_currentAcceleration,2) + " s";
        weightText.text = _currentWeight + " kg";
        priceText.text = _currentPrice+ " €";
    }

    public void RemoveGain(GameObject part)
    {
        if (part == null)
            return;
        CarPartsStats carPartsStats = part.GetComponent<CarPartsStats>();
        if (carPartsStats == null)
            return;
        
        _currentTopSpeed -= carPartsStats.topSpeedGain;
        _currentAcceleration -= carPartsStats.accelerationGain;
        _currentWeight -= carPartsStats.weightGain;
        _currentPrice -= carPartsStats.priceGain;
    }
}
