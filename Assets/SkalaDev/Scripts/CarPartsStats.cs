using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPartsStats : MonoBehaviour
{
    public int topSpeedGain = 0;
    public float accelerationGain = 0.0f;
    public int weightGain = 0;
    public int priceGain = 0;
}
