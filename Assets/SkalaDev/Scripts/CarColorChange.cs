using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarColorChange : MonoBehaviour
{
    public List<Material> mColourSet = new List<Material>();
    public MeshRenderer carMeshRenderer;

    public void SetColourByIndex(int index)
    {
        //Debug.Log("Set Material index: " + index);
        if (mColourSet.Count == 0)
        {
            Debug.LogWarning("Empty body Colour sets.");
            return;
        }

        if (index >= mColourSet.Count)
        {
            Debug.LogError("Out of index.");
            return;
        }
        SetMaterial(mColourSet[index]);
    }
    
    private void SetMaterial(Material lMaterial)
    {
        if (carMeshRenderer != null)
        {
            carMeshRenderer.material = lMaterial;
        }
    }
}
