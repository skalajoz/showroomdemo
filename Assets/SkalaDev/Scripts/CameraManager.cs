using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CameraSets
{
    public GameObject CinemachineCamera;
}

public class CameraManager : MonoBehaviour
{
    public static bool IsPlayerCam; 
    public int InitCamIndex = 0;
    public List<CameraSets> cinemachineCameraSets = new List<CameraSets>();
    
    public void CameraOne()
    {
        IsPlayerCam = false;
        ChangeCamera(cinemachineCameraSets[0].CinemachineCamera);
    }
    
    public void CameraTwo()
    {
        IsPlayerCam = false;
        ChangeCamera(cinemachineCameraSets[1].CinemachineCamera);
    }
    
    private void Start()
    {
        ChangeCameraByIndex(InitCamIndex);
    }

    public void ChangeCameraByIndex(int index)
    {
        if (index < cinemachineCameraSets.Count)
        {
            ChangeCamera(cinemachineCameraSets[index].CinemachineCamera);
        }
    }
    
    private void ChangeCamera(GameObject lCamera)
    {
        if (lCamera == null)
            return;

        foreach (CameraSets curCamSet in cinemachineCameraSets)
        {
            if (curCamSet.CinemachineCamera == cinemachineCameraSets[0].CinemachineCamera)
            {
                continue;
            }

            curCamSet.CinemachineCamera.SetActive(false);
            
        }

        lCamera.SetActive(true);
    }
    
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        
    }
}
