using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuConfigurator : MonoBehaviour
{
    public bool menuVisible = true;
    public KeyCode menuKey;
    public KeyCode exitAppKey;
    public GameObject menuUI;
    public GameObject menuDescription;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(exitAppKey))
        {
            if (Application.isPlaying)
            {
                Application.Quit();
            }
        }

        if (Input.GetKeyDown(menuKey))
        {
            if (menuVisible)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }
    }

    public void Hide()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        menuVisible = false;
        menuUI.SetActive(false);
        menuDescription.SetActive(false);
    }


    public void Show()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        menuVisible = true;
        menuUI.SetActive(true);
        menuDescription.SetActive(true);
    }
}
