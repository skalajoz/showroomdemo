using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitalCamera : MonoBehaviour
{
    public Transform CarSpawn;
    
    public float distance = 5.0f;
    public float YOffset = 0.2f;
    public float cameraSpeed = 2f;

    public float minY = -20f;
    public float maxY = 80f;

    public float minDistance = 0.5f;
    public float maxDistance = 15f;
    
    float x = 0.0f;
    float y = 0.0f;

    
    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360f)
            angle += 360f;
        if (angle > 360f)
            angle -= 360f;

        return Mathf.Clamp(angle, min, max);
    }

    // Update is called once per frame
    void Update()
    {
        if (CarSpawn != null)
        {
            if (Input.GetMouseButton(1)) { 
                x += Input.GetAxis("Mouse X") * cameraSpeed;
                y -= Input.GetAxis("Mouse Y") * cameraSpeed;
            }

            distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 50 * (distance / 100), minDistance, maxDistance);
        }
    }
    
    private void LateUpdate()
    {
        if (CarSpawn != null)
        {
            y = ClampAngle(y, minY, maxY);

            Quaternion rotation = Quaternion.Euler(y, x, 0);
            Vector3 position = rotation * new Vector3(0.0f, 0.0f, -distance) + (CarSpawn.position + new Vector3(0f, YOffset, 0f));

            transform.rotation = rotation;
            transform.position = position;
        }
    }
}
