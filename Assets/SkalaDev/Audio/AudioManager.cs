using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instace { get; private set; }

    public AudioSource ambientSource;

    
    public AudioClip onClickClip;
    public AudioClip onLights;
    public AudioClip onScreenshot;
    
    private AudioSource audioSource;
    
    private void Awake()
    {
        if (Instace != null && Instace != this)
        {
            Destroy(this);
        }
        else
        {
            Instace = this; 
        }
    }
    
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayOnClickSound()
    {
        audioSource.clip = onClickClip;
        audioSource.Play();
    }

    public void PlayOnLights()
    {
        audioSource.clip = onLights;
        audioSource.Play();
    }

    public void PlayOnScreenshot()
    {
        audioSource.clip = onScreenshot;
        audioSource.Play();
    }

    //TODO smooth transition
    public void ChangeAmbientToInterior(bool interior)
    {
        if (interior)
        {
            ambientSource.volume = 0.5f;
        }
        else
        {
            ambientSource.volume = 0.75f;
        }
    }
    
}
